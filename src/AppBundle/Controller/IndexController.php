<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class IndexController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        if ($this->getUser()){
            return $this->redirectToRoute('app_user_profile');
        }
        return $this->render('@App/index.html.twig', array(

        ));
    }

}
