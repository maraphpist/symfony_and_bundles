<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class UserController
 * @package AppBundle\Controller
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/profile")
     */
    public function profileAction()
    {
        $user = $this->getUser();
        $photos = $user->getOwnedPhotos();

        return $this->render('AppBundle:User:profile.html.twig', array(
            'user' => $user->getUsername(),
            'photos' => $photos
        ));
    }

    /**
     * @Route("/editp")
     */
    public function editpAction()
    {
        return $this->render('AppBundle:User:editp.html.twig', array(
            // ...
        ));
    }

}
